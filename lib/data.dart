import 'package:dio/dio.dart';

class StudentData {
  final int id;
  final String firstName;
  final String lastName;
  final String course;
  final String createdAt;
  final int score;
  final String updatedAt;

  StudentData(this.id, this.firstName, this.lastName, this.course,
      this.createdAt, this.score, this.updatedAt);

  StudentData.fromjason(Map<String, dynamic> jason)
      : id = jason["id"],
        firstName = jason["first_name"],
        lastName = jason["last_name"],
        course = jason["course"],
        score = jason["score"],
        createdAt = jason["created_at"],
        updatedAt = jason["updated_at"];
}

class HttpClient {
  static Dio instance =
      Dio(BaseOptions(baseUrl: 'http://expertdevelopers.ir/api/v1/'));
}

Future<List<StudentData>> getstudents() async {
  final response = await HttpClient.instance.get('experts/student');
  print(response.data);

  final List<StudentData> students = [];
  if (response.data is List<dynamic>) {
    (response.data as List<dynamic>).forEach((element) {
      students.add(StudentData.fromjason(element));
    });
  }
  print(students.toString());
  return students;
}

Future<StudentData> saveStudent(
    String firstname, String lastname, String course, int score) async {
  final response = await HttpClient.instance.post('experts/student', data: {
    "first_name": firstname,
    "last_name": lastname,
    "course": course,
    "score": score
  });
  if (response.statusCode == 200) {
    return StudentData.fromjason(response.data);
  } else {
    throw Exception();
  }
}
